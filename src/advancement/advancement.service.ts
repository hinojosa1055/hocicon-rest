import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { DatabaseService } from 'src/database/database.service';

@Injectable()
export class AdvancementService {
  constructor(private readonly dataBaseService: DatabaseService){}

  async create(createAdvancementDto: Prisma.AdvancementCreateInput) {
    return this.dataBaseService.advancement.create({
      data: createAdvancementDto
    })
  }

  async findAll() {
    return this.dataBaseService.advancement.findMany()
  }

  async findByTitle(title: string) {
    return this.dataBaseService.advancement.findMany({
      where: {
        title: {
          contains: title,
        },
      },
    });
  }

  async findByLocation(location: string) {
    return this.dataBaseService.advancement.findMany({
      where: {
        location: {
          contains: location,
        },
      },
    });
  }

  async findByDateRange(startDate: Date, endDate: Date) {
    return this.dataBaseService.advancement.findMany({
      where: {
        createdAt: {
          gte: startDate,
          lte: endDate,
        },
      },
    });
  }

  async findOne(id: number) {
    return this.dataBaseService.advancement.findUnique({
      where: { id },
    });
  }

  async update(id: number, updateAdvancementDto: Prisma.AdvancementUpdateInput) {
    return this.dataBaseService.advancement.update({
      where: { id },
      data: updateAdvancementDto,
    });
  }

  async remove(id: number) {
    return this.dataBaseService.advancement.delete({
      where: { id },
    });
  }
}
