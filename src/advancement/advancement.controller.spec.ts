import { Test, TestingModule } from '@nestjs/testing';
import { AdvancementController } from './advancement.controller';
import { AdvancementService } from './advancement.service';

describe('AdvancementController', () => {
  let controller: AdvancementController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AdvancementController],
      providers: [AdvancementService],
    }).compile();

    controller = module.get<AdvancementController>(AdvancementController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
