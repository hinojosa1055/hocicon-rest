import { Module } from '@nestjs/common';
import { AdvancementService } from './advancement.service';
import { AdvancementController } from './advancement.controller';
import { DatabaseModule } from 'src/database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [AdvancementController],
  providers: [AdvancementService],
})
export class AdvancementModule {}
