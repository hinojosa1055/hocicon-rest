import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { AdvancementService } from './advancement.service';
import { Prisma } from '@prisma/client';

@Controller('advancement')
export class AdvancementController {
  constructor(private readonly advancementService: AdvancementService) {}

  @Post()
  create(@Body() createAdvancementDto: Prisma.AdvancementCreateInput) {
    return this.advancementService.create(createAdvancementDto);
  }

  @Get()
  findAll(@Query('role')role?: 'CLIENT' | 'ADMIN') {
    return this.advancementService.findAll();
  }

  @Get('/location/:location')
  findByLocation(@Param('location') location: string) {
    return this.advancementService.findByLocation(location);
  }

  @Get(':title')
  findTitle(@Param('title') title: string) {
  return this.advancementService.findByTitle(title);
}

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.advancementService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateAdvancementDto: Prisma.AdvancementUpdateInput) {
    return this.advancementService.update(+id, updateAdvancementDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.advancementService.remove(+id);
  }

  @Get('date-range')
  findByDateRange(
    @Query('startDate') startDate: string,
    @Query('endDate') endDate: string,
  ) {
    const start = new Date(startDate);
    const end = new Date(endDate);
    console.log(start)
    return this.advancementService.findByDateRange(start, end);
  }
}
