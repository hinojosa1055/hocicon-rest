import { Test, TestingModule } from '@nestjs/testing';
import { AdvancementService } from './advancement.service';

describe('AdvancementService', () => {
  let service: AdvancementService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AdvancementService],
    }).compile();

    service = module.get<AdvancementService>(AdvancementService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
