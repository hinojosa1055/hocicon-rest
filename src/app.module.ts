import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AdvancementController } from './advancement/advancement.controller';
import { AdvancementService } from './advancement/advancement.service';
import { DatabaseModule } from './database/database.module';
import { AdvancementModule } from './advancement/advancement.module';

@Module({
  controllers: [AppController, AdvancementController],
  providers: [AppService, AdvancementService],
  imports: [DatabaseModule, AdvancementModule],
})
export class AppModule {}
