-- CreateEnum
CREATE TYPE "Role" AS ENUM ('CLIENT', 'ADMIN');

-- CreateTable
CREATE TABLE "Advancement" (
    "id" SERIAL NOT NULL,
    "title" TEXT NOT NULL,
    "mainImage" TEXT,
    "publicationDate" TIMESTAMP(3) NOT NULL,
    "location" TEXT NOT NULL,
    "author" TEXT NOT NULL,
    "content" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Advancement_pkey" PRIMARY KEY ("id")
);
