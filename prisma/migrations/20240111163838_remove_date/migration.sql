/*
  Warnings:

  - You are about to drop the column `publicationDate` on the `Advancement` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Advancement" DROP COLUMN "publicationDate";
